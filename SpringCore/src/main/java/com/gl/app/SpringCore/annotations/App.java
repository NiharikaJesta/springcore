
package com.gl.app.SpringCore.annotations;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {
	public static void main(String[] args) {
		
		ApplicationContext cont=new AnnotationConfigApplicationContext(EmployeeConfig.class);
		Employee eobj=(Employee)cont.getBean("empobj1");
		System.out.println("First name is "+eobj.getEmpname());
		Department d=(Department) cont.getBean("dept");
		System.out.println("Deapartment name"+d.getDeptname());
		System.out.println("Employees in department");
		java.util.List<Employee> list1=d.getEmplist();
		list1.forEach(list->{
			System.out.println("Deapartment name is"+list.getEmpname());
			
		});
//		for(Employee e:list1)
//			
//		{
//			System.out.println("Name is "+e.getEmpname());
//		}
	}

}


