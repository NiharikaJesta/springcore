package com.gl.app.SpringCore;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.gl.app.SpringCore.annotations.dataconnect.SpringJDBCConfiguration;
import com.gl.app.SpringCore.service.ProductService;

public class ProductMain {
	public static void main(String[] args) {
		ApplicationContext cont=
				new AnnotationConfigApplicationContext(SpringJDBCConfiguration.class);
		ProductService pservice=(ProductService)cont.getBean(ProductService.class);
		//pservice.insertData();
		//pservice.deleteData();
		//pservice.updateData();
		pservice.displayDate();
	}

}
