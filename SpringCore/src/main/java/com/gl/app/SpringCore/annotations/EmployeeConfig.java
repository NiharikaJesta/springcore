package com.gl.app.SpringCore.annotations;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EmployeeConfig {
	@Bean(name="empobj")
	public Employee getEmp()
	{
		Employee e=new Employee();
		e.setEmpid(1);
		e.setEmpname("Namita");
		e.setSalary(3433);
		return e;
		
	}
	
	@Bean(name="empobj1")
	public Employee getEmp1()
	{
		Employee e=new Employee();
		e.setEmpid(2);
		e.setEmpname("Manjula");
		e.setSalary(76326);
		return e;
		
	}
	
	
	@Bean(name="dept")
	public Department getDept()
	{
		Department d1=new Department();
		d1.setDeptcode(1001);
		d1.setDeptname("HR");
		return d1;

	}
	@Bean(name="elist")
	public List<Employee> getEmplist()
	{
			List<Employee> emplist=new ArrayList<Employee>();
			Employee e1=new Employee();
			e1.setEmpid(1);
			e1.setEmpname("ABC");
			e1.setSalary(2233.33);
			Employee e2=new Employee();
			e2.setEmpid(2);
			e2.setEmpname("Varsha");
			e2.setSalary(234234);
			emplist.add(e1);
			emplist.add(e2);
			return emplist;
			
		
	}
}
/*
 * **/


