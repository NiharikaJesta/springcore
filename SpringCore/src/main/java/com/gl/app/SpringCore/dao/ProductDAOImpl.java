package com.gl.app.SpringCore.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
//import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.gl.app.SpringCore.pojo.Product;

public class ProductDAOImpl implements ProductDAO{
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	private JdbcTemplate jdbcTemplate;
	
	//@Override
	public void insert(Product p) {
		String query="insert into Product values(?,?,?)";
		jdbcTemplate.update(query,p.getProductid(),p.getProductname(),p.getPrice());
		System.out.println("Inserted succesfully");
}

	//@Override
	public Product getProduct() {
		
		return null;
	}

	public void delete(Product p) {
		String query="delete from product where productid=?";
		jdbcTemplate.update(query,p.getProductid());
		System.out.println("Deleted successfully");
	}

	public void update(Product p) {
		String query="update product set productname=?,price=? where productid=?";
		jdbcTemplate.update(query,p.getProductname(),p.getPrice(),p.getProductid());
		System.out.println("updated successfully");
	}
	
	
	public List<Product> display(){ 
		String query1="select * from product";
	    return jdbcTemplate.query(query1,new RowMapper<Product>(){    
	        public Product mapRow(ResultSet rs, int row) throws SQLException {    
	        	Product p=new Product();
	            p.setProductid(rs.getInt("productid"));
	            p.setProductname(rs.getString("productname"));
	            p.setPrice(rs.getDouble("price"));
	            return p;    
	        }    
	    });    
	}    
}
