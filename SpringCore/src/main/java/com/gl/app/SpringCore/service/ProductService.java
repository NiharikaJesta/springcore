package com.gl.app.SpringCore.service;
import java.util.*;

import com.gl.app.SpringCore.dao.ProductDAO;
import com.gl.app.SpringCore.dao.ProductDAOImpl;
import com.gl.app.SpringCore.pojo.Product;
public class ProductService {
	private Scanner sc;
	private Product p;
	public void setPdaoimpl(ProductDAOImpl pdaoimpl) {
		this.pdaoimpl = pdaoimpl;
	}
	private ProductDAOImpl pdaoimpl;
	public ProductService()
	{
		sc=new Scanner(System.in);
	}
	public void insertData()
	{
		p=new Product();
		System.out.println("Enter product id ");
		p.setProductid(sc.nextInt());
		System.out.println("Enter Product name ");
		p.setProductname(sc.next());
		System.out.println("Enter pridce ");
		p.setPrice(sc.nextDouble());
		pdaoimpl.insert(p);
	}
	public void deleteData()
	{
		p=new Product();
		System.out.println("Enter product id to delete");
		int pid =sc.nextInt();
		p.setProductid(pid);
		pdaoimpl.delete(p);
	}
	public void updateData()
	{
		p=new Product();
		System.out.println("Enter product id to update");
		int pid =sc.nextInt();
		p.setProductid(pid);
		System.out.println("Enter product name to update");
		p.setProductname(sc.next());
		System.out.println("Enter price to update");
		p.setPrice(sc.nextDouble());
		pdaoimpl.update(p);
	}
	public List<Product> displayDate()
	{
		List<Product> prdlist=pdaoimpl.display();
		p=new Product();
		int count=0;
		for(Product p:prdlist)
		{
			count=count+1;
			System.out.println(count);
			System.out.println("ProductId  :"+p.getProductid());
			System.out.println("productname:"+p.getProductname());
			System.out.println("price      :"+p.getPrice());
			
		}
		return prdlist;
		
	}
}
