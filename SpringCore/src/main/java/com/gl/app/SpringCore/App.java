package com.gl.app.SpringCore;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	ApplicationContext appcont=new ClassPathXmlApplicationContext("Beans.xml");
//    	Employee emp=(Employee)appcont.getBean("empobj");
//    	System.out.println("Employee name is "+emp.getEmpname());
//    	System.out.println("Employee salry is "+emp.getSalary());
    	
    	 Department dept=(Department)appcont.getBean("deptobj");
         System.out.println("Department code is:"+dept.getDeptcode());
         System.out.println("Department name is:"+dept.getDeptname());
         List<Employee> elist=dept.getEmplist();
         for(Employee e:elist) {
             System.out.println("Employee Name is:"+e.getEmpname());
         }
    }
}

