package com.gl.app.SpringCore.dao;

import java.util.List;

import com.gl.app.SpringCore.pojo.Product;

public interface ProductDAO {
	public void insert(Product p);
	public Product getProduct();
	public void delete(Product p);
	public void update(Product p);
	public List<Product> display();
}
