package com.gl.app.SpringCore.annotations.dataconnect;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.gl.app.SpringCore.dao.ProductDAOImpl;
import com.gl.app.SpringCore.service.ProductService;

@Configuration
public class SpringJDBCConfiguration {
	DriverManagerDataSource datasource;
	JdbcTemplate jdbcTemplate;
	ProductDAOImpl pdaoimpl;
	@Bean
	public DataSource dataSource()
	{
		
		datasource=	new DriverManagerDataSource();
		datasource.setDriverClassName("com.mysql.cj.jdbc.Driver");
		datasource.setUrl
		("jdbc:mysql://localhost:3306/SpringCore");
		datasource.setUsername("root");
		datasource.setPassword("Neeha@hcl10");
		return datasource;
		
	}
	@Bean
	public JdbcTemplate jdbcTemplate()
	{
		 jdbcTemplate=new JdbcTemplate();
		jdbcTemplate.setDataSource(datasource);
		return jdbcTemplate;
		
	}
	@Bean
	public ProductDAOImpl productDAO()
	{
		pdaoimpl=new ProductDAOImpl();
		pdaoimpl.setJdbcTemplate(jdbcTemplate);
		return pdaoimpl;
	}
	@Bean
	public ProductService productService()
	{
		ProductService pservice=new ProductService();
		pservice.setPdaoimpl(pdaoimpl);
		return pservice;
	}
}
